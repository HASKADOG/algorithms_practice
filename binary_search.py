# binary_search

numbers = [1, 2, 4, 5, 7, 9, 13, 15, 17, 19, 20]


def binary_search(target, key):
    low = 0
    high = len(target) - 1

    while low <= high:
        mid = (low + high) // 2
        guess = target[mid]

        if guess == key:
            return mid

        if guess < key:
            low = mid + 1

        if guess > key:
            high = mid - 1


print(binary_search(numbers, 20))
